# Overview
zpydify is an opinionated, convenience wrapper around gulp.

it presents a number of configuration objects that are used in executing defined
gulp tasks.

#Usage
To use zpydify:
1. `npm i -D gulp zpydify`
2. add a gulpfile.js to the root of your project, for example:

```
'use strict';

const gulp = require('gulp'),
    Zpydify = require('./zpydify');

const serverBuild = new Zpydify('./server');

gulp.task('default', ['launch']);
gulp.task('launch', ['watch'], () => serverBuild.launch());


/*
* WATCHERS
*/
gulp.task('watch', ['watch-app-ts', 'watch-test-ts', 'watch-views', 'watch-styles']);
gulp.task('watch-test-ts', ['compile-test-ts'], () => gulp.watch(
    gulp.src.testTs, ['compile-test-ts']));
gulp.task('watch-app-ts', ['compile-app-ts'], () => gulp.watch(
    gulp.src.appTs, ['compile-app-ts']));
gulp.task('watch-views', ['compile-views'], () => gulp.watch(
    serverBuild.src.views, ['compile-views']));
gulp.task('watch-styles', ['compile-styles'], () => gulp.watch(
    serverBuild.src.styles, ['compile-styles']));

/*
* LINTERS
*/
gulp.task('lint-app-ts', () => serverBuild.lintTs(serverBuild.src.appTs));
gulp.task('lint-test-ts', () => serverBuild.lintTs(serverBuild.src.testTs));

/*
* CLEANERS
*/
gulp.task('clean-app-js', () => serverBuild.clean(serverBuild.dest.appJs));
gulp.task('clean-test-js', () => serverBuild.clean(serverBuild.dest.testJs));
gulp.task('clean-views', () => serverBuild.clean(serverBuild.dest.views));
gulp.task('clean-styles', () => serverBuild.clean(serverBuild.dest.styles));

/*
* COMPILERS
*/
gulp.task('compile-app-ts', ['lint-app-ts', 'clean-app-js'], () => serverBuild.compileTs(
    serverBuild.src.appTs, serverBuild.dest.appJs));
gulp.task('compile-test-ts', ['lint-test-ts', 'clean-test-js'], () => serverBuild.compileTs(
    serverBuild.src.testTs, serverBuild.dest.testJs));
gulp.task('compile-views', ['clean-views'], () => serverBuild.compileJade(
    serverBuild.src.views, serverBuild.dest.views));
gulp.task('compile-styles', ['clean-styles'], () => serverBuild.move(
    serverBuild.src.styles, serverBuild.dest.styles));
```

# Configuration objects
The following are standard configuration object that exist as properties of
a Zpydify instance. The only variable that currently exists in these objects
is the `projectFolder`, which is passed in via the constructor.

```
this.rootPath = projectFolder;
```

```
this.src = {
        appTs: [
            projectFolder + '/src/**/*.ts',
            '!' + projectFolder + './src/**/*.spec.ts',
            './typings/main.d.ts'
        ],
        testTs: [
            projectFolder + '/src/**/*.spec.ts',
            './typings/main.d.ts'
        ],
        views: [
            projectFolder + '/src/views/**/*.jade'
        ],
        styles: [
            projectFolder + '/src/assets/styles/**/*.css'
        ]
    };
```

```
this.dest = {
        appJs: projectFolder + '/compiled/js',
        testJs: projectFolder + '/compiled/test',
        views: projectFolder + '/compiled/views',
        styles: projectFolder + '/compiled/styles'
    };
```

```
this.options = {
        node: {
            script: projectFolder + '/compiled/js/index.js',
            delayTime: 1,
            env: {
                PORT: 3283,
                NODE_ENV: 'dev'
            },
            watch: projectFolder + '/compiled/js'
        },
        sourcemaps: {
            path: '.',
            init: {},
            write: {}
        },
        typescript: {
            target: 'es5',
            module: 'commonjs',
            moduleResolution: 'node',
            noImplicitAny: true,
            removeComments: false
        },
        tslint: {},
        tslintStylish: {
            emitError: false,
            sort: true,
            bell: false,
            fullPath: true
        },
        jade: {
            pretty: true
        }
    };
    ```
Future versions will allow users to manipulate these configuration objects.