'use strict';
const
    path = require('path'),
    args = require('yargs').argv,
    del = require('del'),
    browserSync = require('browser-sync'),
    gulp = require('gulp'),
    plug = require('gulp-load-plugins')({ lazy: true });

const nodePort = 3283;
let testServer = true;

const zLog = function(color, msg) {
    if (typeof (msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                plug.util.log(plug.util.colors[color](msg[item]));
            }
        }
    } else {
        plug.util.log(plug.util.colors[color](msg));
    }
}

const Zpydify = function(projectFolder, moduleEngine, color) {
    this.rootPath = projectFolder;
    this.projectColor = color || 'bgWhite';
    this.src = {
        appTs: [
            projectFolder + '/src/**/*.ts',
            '!' + projectFolder + './src/**/*.spec.ts',
            './typings/main.d.ts'
        ],
        testTs: [
            projectFolder + '/src/**/*.spec.ts',
            './typings/main.d.ts'
        ],
        views: [
            projectFolder + '/src/**/*.jade'
        ],
        styles: [
            projectFolder + '/src/assets/styles/**/*.css'
        ]
    };
    this.dest = {
        appJs: projectFolder + '/compiled/app',
        testJs: projectFolder + '/compiled/test',
        views: projectFolder + '/compiled/views',
        styles: projectFolder + '/compiled/styles'
    };
    this.options = {
        node: {
            script: projectFolder + '/compiled/app/index.js',
            delayTime: 1,
            env: {
                PORT: nodePort,
                NODE_ENV: 'dev'
            },
            watch: projectFolder + '/compiled/app'
        },
        sourcemaps: {
            path: '.',
            init: {},
            write: {}
        },
        typescript: {
            target: 'es5',
            module: moduleEngine,
            moduleResolution: 'node',
            noImplicitAny: false,
            removeComments: false,
            emitDecoratorMetadata: true,
            experimentalDecorators: true
        },
        tslint: {},
        tslintStylish: {
            emitError: false,
            sort: true,
            bell: false,
            fullPath: true
        },
        jade: {
            pretty: true
        },
        browserSync: {
            proxy: `localhost:${nodePort}`,
            port: 4242,
            files: ['client/compiled/**/*.*'],
            ghostMode: {
                click: true,
                location: false,
                forms: true,
                scroll: true
            },
            injectChanges: true,
            logFileChanges: true,
            logLevel: 'info',
            logPrefix: 'browser-sync',
            notify: false,
            reloadDelay: 500
        },
        mocha: {
            reporters: 'spec',
            bail: true
        }
    };
    zLog(this.projectColor, `\n-----------------------------------------------------\n*** STARTING ZPYDIFICATION IN FOLDER ${this.rootPath} ***\n-----------------------------------------------------`)
};


Zpydify.prototype.clean = function(path) {
    const self = this;
    zLog(self.projectColor, `CLEANING: ${this.rootPath}`);
    zLog('white', `${path}`);
    return function() {
        return del(path);
    } ()
};

Zpydify.prototype.move = function(src, dest) {
    const self = this;
    zLog(self.projectColor, `MOVING: ${this.rootPath}`);
    zLog('white', `${src}`);
    zLog('white', `--> ${dest}`);
    return function() {
        return gulp.src(src)
            .pipe(gulp.dest(dest));
    } ()
};

Zpydify.prototype.lintTs = function(src) {
    const self = this;
    zLog(self.projectColor, `LINTING: ${this.rootPath}`);
    zLog('white', `${src}`);
    return function() {
        return gulp.src(src)
            .pipe(plug.tslint(self.options.tslint))
            .pipe(plug.tslint.report(plug.tslintStylish, self.options.tslintStylish));
    } ()
};

Zpydify.prototype.compileTs = function(src, dest) {
    const self = this;
    zLog(self.projectColor, `COMPILING: ${this.rootPath}`);
    zLog('white', `${src}`);
    zLog('white', `--> ${dest}`);
    return function() {
        return gulp.src(src)
            .pipe(plug.if(args.files, plug.print()))
            .pipe(plug.sourcemaps.init(self.options.sourcemaps.init))
            .pipe(plug.typescript(self.options.typescript))
            .pipe(plug.sourcemaps.write(self.options.sourcemaps.path, self.options.sourcemaps.write))
            .pipe(gulp.dest(dest));
    } ()
};

Zpydify.prototype.compileJade = function(src, dest) {
    const self = this;
    zLog(self.projectColor, `COMPILING: ${this.rootPath}`);
    zLog('white', `${src}`);
    zLog('white', `--> ${dest}`);
    return function() {
        return gulp.src(src)
            .pipe(plug.jade(self.options.jade))
            .pipe(gulp.dest(dest))
    } ()

};

Zpydify.prototype.mocha = function(src) {
    const self = this;
    zLog(self.projectColor, `TESTING: ${this.rootPath}`);
    return function() {
        return gulp.src(src + '/**/*.spec.js', { read: false })
            .pipe(plug.mocha(self.options.mocha))
    } ()
};

Zpydify.prototype.karma = function(root) {
    const self = this;
    zLog(self.projectColor, `TESTING: ${this.rootPath}`);
    zLog('bgWhite', `KARMA IMPLEMENTATION GOES HERE...`);
    return function(done) {
        return
        // new KarmaServer.start({
        // }, done);
    } ()
}

Zpydify.prototype.launch = function() {
    const self = this;
    return function() {
        return plug.nodemon(self.options.node)
            .on('start', function() {
                if (args.nosync || browserSync.active) {
                    return;
                }

                browserSync(self.options.browserSync);
            })
            .on('restart', function() {
                setTimeout(function() {
                    browserSync.notify('browser-sync reloading...');
                    browserSync.reload({ stream: false });
                }, 1000);
            });
    } ()
};

module.exports = Zpydify;